---
title: Essay
---

### 2023/10/12 天气阴

今天给农科院基因组研究所的黄三文老师发了邮件和简历，没想到才没过多久就有一位黄老师的博士生加我微信，说是要我做一个 presentasion，了解一下我，也跟我讲了讲黄老师课题组的一些事情。在于这位师兄交流过后，我总结了下我目前的一些不足：

- 英语读写能力不足
- 欠缺一些生物学知识
- 生物信息学分析过度依赖软件（TBtools 等）
- 科研项目的参与度不高
- 生物信息学分析深度不够，多在基因层面

这些问题也需要迫切的解决。其实自从了解了一些生物信息学相关的东西之后我也发现了上述问题，也在积极地做出改变，例如仓库地 README 尽量用英文去写；Blog 的 About 页面也用英文；查找文献时尽量 NCBI or EI 等检索；学习 R 语言来进行生信绘图等，但似乎进度还是很慢，成果没有展现出来。

似乎需要请黄老师帮忙了，之前黄老师说要与西南科大的一个生信的老师合作，说不定可以让我过去学习，有时间再询问下黄老师吧。另外，也需要多进进实验室了，目前更多的生信分析是与生物或环境实验相结合的，因此只会生信分析的话是不够的。

还是得继续加油啊~

---

今天无意中读到一篇文章 [《People Without Social Media: Why They Don’t Use It》](https://dataoverhaulers.com/people-without-social-media/) 感觉有点启发，我们似乎花了太多时间在社交媒体上，比如微信、抖音、QQ 之类的，于是我也查看了一下我的软件使用时间：

<img src="https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202310121224499.png" width=250px/>

碎片化的阅读和浏览似乎对我们专注做一件事有着巨大的负面影响，试问现在有多少人能够静下心来看完一本书？

但有些软件似乎我们也是离不开的，比如我：

- 微信：与朋友、家人等交流的主力军
- QQ：接受学校通知比赛通知等
- 企业微信：课题组相关事务

...

于是我仔细想了一下，有一些似乎也是可以舍去的，于是我删掉了下载没几天的抖音，删除了没有必要的联系人，退出了没用的群聊，将群聊都折叠起来 ...

我希望的是能够专注的做一件事，尽量减少来自外界的干扰，而目前我们所困扰的就是来自手机，繁多的通知、消息不断的群聊之类的，那么似乎就不难，把它们全部想办法解决掉就是了，尽可能不下载没必要的 APP 等。

在我们国家目前，完全删掉社交软件似乎是不可能的，所以也就只能尽量来减少社交软件给我们带来的负面影响，所谓戒网瘾。

除此之外，国内的软件似乎大都在向一个趋势发展，那就是一个 APP 有 N 多种功能，试问我们生活常用的软件有几个不能刷视频、聊天？QQ 就是一个典型的例子，什么小世界、鹅毛集市、直播 ... 

让人一言难尽，下载了一个 APP 就像下载了好多个一样，对于这一点，实在不知道说什么好了。

### 2023/10/11 天气阴

今天打算使用本地安装 pfam_scan 在 Pfam 的数据库中搜索一系列基因的结构域，死活安装不上，conda 安装的话一直解决不了环境问题，如果在 Pfam 的 [ftp](ftp://ftp.ebi.ac.uk/pub/databases/Pfam/Tools/) 上下载安装包使用的话，又缺少 Perl 的依赖，只能放弃。

后来使用 [hmmer 网页工具](https://www.ebi.ac.uk/Tools/hmmer/) 解决问题，并且收到邮件回复后使用 TBtools 可视化，但是可视化效果并不好，保存为 jpg 或 png 格式是看不清楚的，而使用 svg 格式保存，再用 IE 打开查看，效果很完美。

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202310111716133.png)

---

从童年起，

我便独自一人，照顾着历代的星辰。

但遇见你以后，念念落地生根。

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202310112157941.png)

### 2023/10/10 天气阴

今天蒋文君师姐说要尝试一下使用 pfam 的数据库来进行结构域注释，因为之前常用的都是 NCBI 或者 SMART 网站的，由于数据库之间的差异，hmmer 根据结构域检索的家族基因在 NCBI 的数据库中不一定都具有相同的结构域，所以使用同一数据库的工具和方法进行检索可以极大地避免这种情况。

找到一个 [pfam_scan](https://www.ebi.ac.uk/Tools/pfa/pfamscan/) 工具可以批量注释结构域，本想使用网页版速速解决了，但网页版输出的结果格式是打乱的。

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202310102209258.png)

而本地自己构建的话，数据量又很大，Pfam-A.hmm 有 1.5GB 左右，只能在超算上做了，于是用 wget 把下载挂到后台，准备睡觉去了~

---

今天完成了对 [auto-hmmer](https://github.com/YuanJ2003/auto-hmmer) 的重构，由于 pfam 数据库迁移到 [interpro](https://www.ebi.ac.uk/interpro/) 了，不得不更改数据来源的 url，这里其实新版和旧版数据库差不多，在数据的下载链接里只有 PF 编号不同，但新版是压缩文件，使用 Python 调用 wget 库下载后，再使用 gzip 库解压，把内容写入指定的文件即可。

```python
def data_download(PF_number):
    import wget
    hmm_url = "https://www.ebi.ac.uk/interpro/wwwapi//entry/pfam/{}?annotation=hmm".format(PF_number)
    wget.download(hmm_url, 'hmm.gz')

    PFseed_url = "https://www.ebi.ac.uk/interpro/wwwapi//entry/pfam/{}/?annotation=alignment:seed&download".format(PF_number)
    wget.download(PFseed_url, 'PFseed.gz')

def decompress_data():
    import gzip

    with gzip.open('hmm.gz', 'rb') as f:
        content = f.read().decode('utf-8')
    with open('./model.hmm', 'w') as f:
        f.write(content)

    with gzip.open('PFseed.gz', 'rb') as f:
        content = f.read().decode('utf-8')
    with open('./PFseed.txt', 'w') as f:
        f.write(content)
```

同时我把所有的功能模块放入一个 modules 文件夹中，再通过 main.py 分别调用功能，这样就不再依赖于 CI/CD 来完成自动化流程，因此也可以在本地环境更方便的使用。

### 2023/10/09 有雨

你会不会在下一首诗里，

想起从前的月亮，

还是说，

我只是你历史上，

盛了一半的唐。

---

今日思考了 Python 水稻基因工具箱的一些事情，想着还是加入 CDS、DNA 序列的提取，但这俩文件比较大，先调用 Python 从云存储下载后再使用似乎更好。另外，pfam 数据库已经更新了，也就意味着之前 [auto-hmmer](https://github.com/YuanJ2003/auto-hmmer) 项目也需要进行进一步的更新了，并且似乎可以重构一下，方便引入新的 hmmer 命令，后面慢慢搞吧~

### 2023/10/08 天气阴

饿了么的 CDN 挺久之前似乎就不同步新发布的 npm 包了，但以前的还可以用，公益 CDN 就是存在极大的不稳定性，于是收集了一波 CDN，以备用。

能够同步自己在 npm 发布的包的 CDN：

```txt
https://unpkg.com/
http://npm.elemecdn.com/
https://cdn.onmicrosoft.cn/
https://npm.onmicrosoft.cn/
https://cdn.jsdelivr.net/npm
```

静态资源公共库镜像：

```txt
https://www.bootcdn.cn/
https://staticfile.org/
https://cdn.baomitu.com/
https://cdn.bytedance.com/
https://cdnjs.com/
```

---

今天对博客主要做了两个改动：

- 引入自定义筑紫 a 丸字体
- 引入 fontawesome 图标

引入字体实际上就是在 main.css 中添加下面的代码：

```css
@font-face {
  font-family: 'ZhuZiAWan';	
  src: url(https://unpkg.com/yuanj2003@1.0.4/public/ZhuZiAWan.ttf);
  font-weight: normal;
  font-style: normal;
  font-display: block;
}
```

随后在对应的位置如导航栏等，引入 font-family 就可以了

```css
section.header nav {
  font-size: 14px;
  font-family: 'ZhuZiAWan';
  margin-bottom: 16px;
}
```

而 fontawesome 图标库则是在页脚（为保证每个页面都加载）引入 css：

```html
<link rel="stylesheet" href="https://lf26-cdn-tos.bytecdntp.com/cdn/expire-1-M/font-awesome/6.0.0/css/all.min.css">
```

然后在需要的地方写入图标代码即可

```html
<i class="fa-solid fa-rss" style="color: #000000;"></i>
```

---

今天又想起沈从文：

一个女子在诗人的诗中，永远不会老去，但诗人他自己却老去了。我想到这些，我十分忧郁了。生命原是太脆薄的一种东西，并不比一株花更经得住年月风雨，用对自然倾心的眼，反观人生，使我不能不觉得热情的可珍，而看重人与人凑巧的藤葛。

### 2023/10/07 多云转晴

今天天气真不错~

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202310071452070.png)

---

最近一直在思考一个问题——什么是大学？

梅贻琦说过：“所谓大学者，非谓有大楼之谓也，有大师之谓也”。

蔡元培说过：“大学者，研究高深学问者也”，“大学者，囊括大典网罗众家之学府也”。

儒家散文《大学》提出“三纲领”（明明德、亲民、止于至善）和“八条目”（格物、致知、诚意、正心、修身、齐家、治国、平天下），强调修己是治人的前提，修己的目的是为了治国平天下，说明治国平天下和个人道德修养的一致性。

而今日之大学真是如此？

据我的经验来看，大学里目前可大致分为四类：

- 激进派：即所谓“卷王”，不仅追求学习成绩高，课余也会参加各种比赛来丰富阅历。
- 佛系派：即按时去上课、写作业，但课余摆烂，不追求多高成绩，但追求不挂科、不给自己找事情。
- 发展派：即不好好上课，时间均花在参加比赛、参加社团活动等事务，力求全面发展自身能力（成绩次要）。
- 摆烂派：即不认真上课、完成作业，课余时间皆在进行打游戏等娱乐活动，考试凭运气、同学和老师的“水分”。

而以上四派的比例均会因学校的水平高低有所不同，具体表现为学校越好，激进派越多；学校越差，摆烂派越多。

那为何造成这种情况？

因为身边常常有人说：

- 等你上大学了就轻松了
- 没有逃过课的大学是不完整的
- 没有挂过科的大学是不完整的

因为有很多 B 方案：

- 考试可以靠期末突击
- 挂科了还可以补考
- 毕不了业还有换证考试

因为学习途径多样化：

- 老师讲过的内容网上有
- 老师没有讲过的内容网上也有

因为有自己的判断法则：

- 对于学习有预定期望
- 对于老师有学术期待
- 对于未来有一定畅想

因为缺乏自控力、因为沉不下心学习、因为有许许多多的其他事情要做。.....

今天的大学已经不像《觉醒年代》中那样百家争鸣、尊师重道，因为时代的多样性造成了学生更乐于追求自己所崇尚的那种“道”，而今社会也似乎对学生的要求更多，纸上得来终觉浅，只会学习似乎是行不通的，而所谓的“旁门左道”似乎也并不是一点都行不通的。

如今大学的环境似乎也不是很理想，我们要忙于开各种会、打各种卡、上各种所谓素养课程，就因为社会要求我们要“全面发展”，马克思终其一生研究出的真理我们要在几节课内就掌握；少看一眼 QQ 群就能错过交作业；英语不及格我就不能毕业，哪怕我不考研、考公；我本专业成绩好才能转专业；戏剧鉴赏、影视鉴赏等这些课程我也必须去上 ...

大学生应该追求的究竟是什么？

大学应该是一个升华的殿堂，里面装着无尽的知识，学校提供了平台，我们享受着迄今为止所有教育经历里配套最全、专业门类最全、教育资源最好的待遇，可我们不应该这个殿堂里“酣睡”，这是一种悲哀，真正的“大学”不应该这样。

### 2023/10/06 有雨

之前将 Python ID 转换程序重构了一下，使代码结构更加合理，并且更方便增加新的插件，但是打包时还是遇到了问题，使用 pyinstaller 和 cxfreeze 打包的程序都不能正常运行，输入 ID 之后程序就退出了，目前还没找到好的解决方法，只能先要求用户安装 Python 环境再使用了。我想的另一个解决方法就是在仓库中添加 Python 压缩包，再写一个 bat 脚本来将 Python 解压后放到指定目录，并且把 Python 添加到环境变量，然后再用另一个 bat 脚本来启动程序。但这个方案似乎有些过于麻烦了，只能先这样，后面再解决打包成 exe 程序后闪退的问题了。

### 2023/10/05 天气阴

那一刻，时光之河的并蒂花上，一只蝴蝶对另一只蝴蝶说：”梁兄，别来无恙？

如果存在前生和来世，洛阳有牡丹盛开，济南有荷花凋谢，金陵的梅花飘香，北京的月季绽放，我们前生和来世的家在哪里？

从西晋到东晋，从长安到西安，三生三世，你还在我心底。我们一直形影不离，蝶翅约定了双飞，是谁在亭子里弹琴？

杏花纷纷，纷纷落在地上变成尘埃。从楷书到行书，从长笺到短信，万水千山，我还在你梦里。我们始终没有分别，指尖承诺了同醉，是谁在草桥边送君？

纷纷大雪，大雪铺满归来时的道路。最初的一拜天地，也是最后的一谢天地。

### 2023/10/04 天气阴

感觉这张图有点意思

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202310041705011.png)

---

最近 Cloudflare 更改了策略，加上国内最近似乎又在屏蔽 Cloudflare 的 DNS，手机上一直打不开博客，电脑上有时候也需要刷新几次才能打开 ... 只能将域名接回 DNSpod，主要的两个域名 hieroglyphs.top 和 huangjin-lab.cn 的 DNS 接回去了，其它一些无关紧要的域名还在 Cloudflare 用来绑定 Worker 和 R2 存储桶等。DNS 为什么要接入到 Cloudflare？ 因为 DNSpod 归属腾讯云，腾讯云的界面是在一言难尽，还动不动要验证、登录什么的，实在无语，如果接入 DNSpod 还不行，那可能就是 Vercel 的问题，只能再换别的服务，不过我还是会把 DNS 接到 Cloudflare ... 

群里有些人解析到 Vercel 的域名直接没法访问了，似乎是沿海地区管控比较严格 ... 这确实让人有些“不知所措”。现在有些地方访问 VScode 的官网居然都跳反诈中心了 ...

---

好吧，不是 Cloudflare 的问题，V2EX 上的网友说 Vercel 的 cname.china 也被有些运营商屏蔽了 ...

询问了一下，我的域名在有些地区也是跳转到反诈中心 ...

经过测试，netlify 还是正常的，于是切换过去了。

### 2023/10/03 天气阴

A ∩ B = Ø 

### 2023/10/02 天气阴

今天本打算用 GitHub Action 来自动打包 Python ID 转换程序，但 Pyinstaller 在 Linux 下打包的程序只能在 Linux 下使用 ... 而 Windows 下打包的程序才能用于 Windows，我本意是想对代码做出改进和优化后 push 到仓库自动打包，那么使用的时候就直接下载最新的产物就可以了，可现实是 Python 没有支持交叉编译的跨平台的打包工具，所以 Python 想打包某个平台的可执行文件，就必须在这个平台上配置打包环境。可以用 docker，但似乎有些过于麻烦了，这只是一个几十行代码的脚本，有点本末倒置了。

---

这几天一直是一个人，一个人在寝室、一个人吃饭、一个人看书 ... 今天也一个人去看了下《前任 4》，感觉呢似乎越大越习惯一个人了，从高三开始经常一个人吃饭、做事，感觉效率也很高，当然，有的时候还是会和朋友聚一聚，聊聊天什么的。我觉得我的社交能力并不差，能与身边的朋友、同学相处的融洽，做暑假工的时候也能与所谓同事和领导相处的不错，跟我哥的一些朋友也可以喝几杯、聊几句。有些事情似乎一个人可以做的更好，有些事情也只能一个人做，比如我很喜欢计算机与生信，但我的朋友大部分都根本不了解这些，所以我也只能一个人做。

泰戈尔说过：孤独是一个人的狂欢，狂欢是一群人的孤单

一个人的时候难免会感到孤独，但对于我来说，似乎闲下来无事可做的时候是最孤独的，上学期的时候 3 月份开始从头开始学生信，那会什么都不会，需要学的东西很多，每天都很忙，还要准备考试与比赛，从早忙到晚，感觉非常充实，即使是一个人也感觉很快乐。记得应该是六月 1 号，熬夜写基因家族鉴定的教程写到五点，后来又是改以前的 hexo 博客，边学边改，改了五天，最后看的成果还是感觉很充实。反观国庆这几天，第一天写完了 SAP 文章的 introduction 部分和 Python ID 转换程序，后来就没有很重要的事了，就感觉到心里缺点什么，所以人啊，还是要多多找点事情做~

<!-- 最近与一个学妹一直在聊天，但今天还是断了联系，姑娘说是觉得一个人更好，我也想了想这个问题，这么久以来，我似乎都是一个人，但我还是会希望有一个人能与我共同前进，那种心灵上的共鸣是我所期望的，不过这种事情也是可遇不可求的，我从来不会阻止别人离开，也不抵触别人的到来，是过客还是人生伴侣，时间自会证明一切。 -->

### 2023/10/01 天气阴

露从今夜白，月是故乡明。

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202310012227430.png)

### 2023/09/29 天气阴

今天完成了 SAP 文章 introduction 部分的撰写，并且 Python 转换水稻 ID、提取序列的程序大体也完成了，没有想到今天能完成这两项，后面还需要继续努力。今天主要是在 Python 程序打包时遇见问题，如果 pyinstaller 将程序连同数据一起打包的话，程序运行总会中断，但不带数据打包的话运行正常，后面再慢慢改吧~

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202309292124230.png)

PythonID 转换程序主要面临的问题：

- 没有 GUI 界面，使用的还是终端界面，导致不能直接粘贴 ID 列表，只能一个 ID 进行操作
- 没有连同数据打包成 exe 程序

后面边学习边解决这些问题。

晚上与在学校的几个好友吃了点小吃，喝了点小酒，算是浅过了一个中秋吧。

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202309292125571.png)

总感觉自己喝了点酒之后心里就莫名开始悲哀，但我也沉迷于这种飘忽的状态，不用想很多事，哪怕这么一瞬间，我也觉得很美好，我想我比较爱喝啤酒的原因就是热爱这种飘然、惆怅的境遇吧 ...

### 2023/09/28 天气阴

今天又看到这个 ... 想起一年前因为“劝君更进一杯酒，西出阳关无故人”这一句诗 gitee 仓库被禁止使用 pages，属实无语住了

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202309282046929.png)

又想起了知乎上的一篇 [回答](https://www.zhihu.com/question/63187737/answer/3169369236)

只能说，没话说 ...

### 2023/09/27 天气阴

今天看了一下电影《肖申克的救赎》，这部以监狱为主要场景的电影满片都充斥着温馨的话语，从始至终都在告诉我们：“要怀抱希望”。恐惧让你沦为囚犯，而希望让你重获自由 ...

银行家安迪帮助监狱官避税后，为与自己一同修屋顶的囚犯争得了一些冰啤酒，虽然这似乎并没有什么大用，但确实他们漫长监狱人生中的一束光，即使短暂，但它也来过，正若 Red 所说的：阳光洒肩头 仿佛自由人。短暂的快乐也是快乐。

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202309272253917.png)

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202309272254100.png)

Morgan Freeman 三次假释听证会据说是一起拍的，但 Morgan Freeman 似乎真的在狱中呆了几十年一样，连眼神都在告诉人们他心中的后悔。

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202309272250746.png)

这些人似乎都不理解安迪，安迪一入狱就在救赎，先是救赎自己，再救赎狱友，后来救赎肖申克监狱，人生的过程就是一个救赎的过程，也是一个摆脱 institutionalization（体制化）的过程。

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202309272256649.png)

### 2023/09/26 天气阴

今天晚上又读了一下 [陈铭教授](https://bis.zju.edu.cn/binfo/members/chen_ming.htm) 的 [《生物信息学》](https://book.sciencereading.cn/shop/book/Booksimple/show.do?id=BDAB4DFCD95FA852AE053010B0A0AAE47000)，是在陈程杰老师的推荐下看的，对一些基础知识和算法原理的讲解比较清除，但我还是有很多看不懂的地方，主要是在于对一些生物学和数学知识的欠缺，还需要继续努力。

### 2023/09/25 天气阴

今天给博客增加了两个功能

- MathJax 数学公式支持
- 导航栏显示页面名称

数学公式的话实际上就是在 footer 中引入了插件，并且对样式进行了一些修改，为了保证每一个页面都能成功加载，所以才添加到 footer，代码如下

```html
    <script type="text/javascript"
        async
        src="https://cdn.bootcss.com/mathjax/2.7.3/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
    MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [['$','$'], ['\\(','\\)']],
        displayMath: [['$$','$$'], ['\[\[','\]\]']],
        processEscapes: true,
        processEnvironments: true,
        skipTags: ['script', 'noscript', 'style', 'textarea', 'pre'],
        TeX: { equationNumbers: { autoNumber: "AMS" },
            extensions: ["AMSmath.js", "AMSsymbols.js"] }
    }
    });

    MathJax.Hub.Queue(function() {
        // Fix <codetags after MathJax finishes running. This is a
        // hack to overcome a shortcoming of Markdown. Discussion at
        // https://github.com/mojombo/jekyll/issues/199
        var all = MathJax.Hub.getAllJax(), i;
        for(i = 0; i < all.length; i += 1) {
            all[i].SourceElement().parentNode.className += ' has-jax';
        }
    });
    </script>

    <style>
    code.has-jax {
        font: inherit;
        font-size: 100%;
        background: inherit;
        border: inherit;
        color: #515151;
    }
    </style>
```

而导航栏的修改实际上就是把 head.html 中`<title>{{ .Site.Title }}</title>`改成了`<title>{{ Title }}</title>`，取消始终显示站点 title 而是显示页面 title。

### 2023/09/23 天气阴

今天又下载了之前经常玩的游戏——世界征服者 3，还是有很多回忆的，记得第一次玩这个游戏的时候还是六年级在学习平板上玩的，当时也不太会，后来摸索了挺久才逐渐会了，游戏主要是两种情景模式，征服世界和战役，征服世界主要就是在 1939、1943、1950、1960 这几个年代，按照历史分为几个国家进行战争，跟历史差不多一致；而战役模式则是从二战开始的各个战役，比如闪击波兰、海狮计划、黄色计划等。

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202309271300472.png)

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202309271300207.png)

小时候玩这个也学到了很多东西，比如各个国家的首都（在游戏里就是科技最发达的）还有一些历史事件等，也对二战历史开始感兴趣，后来读过《二战全史》等书，包括高中有一次历史课上演讲我也讲的是二战历史。我本身对历史就比较感兴趣，高三开始就看明朝历史了，看的最多的还是当年明月的《明朝那些事儿》。

这个游戏后来出了第四版，网上也有很多 mod，但玩了好多版本还是感觉原版的 3 好玩，近两年 3 又更新了，主要是支配了新版本安卓系统和全面屏，不然全面屏手机玩旧版本的时候有些按钮显示不全。

### 2023/09/22 小雨

今天早上起来有点发烧，嗓子也很痛，应该是感冒了，所以只上了第一节课，组会都没有开完就坚持不住了，吃了药之后睡了挺久，感觉恢复了好多，晚上和几个同学去打球了，冒雨打球。可能明天会更严重！

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/202309271259736.png)

雨越下越大，没打多久就回去了，然后吃了水果和炒饭，把博客换了主题，今天就这么就过去了~~

### 2023/09/07 天气晴

记一个 zsh 报错：

```bash
zsh: corrupt history file /home/hieroglyphs/.zsh_history
```

解决方法：

```bash
sudo npm install -g zsh_history
zsh_history
```

### 2023/09/05 天气晴

今天用超算的时候发生一个错误，从 NCBI 上下载的数据格式有问题，解压出来的文件不对劲，而且解压出来一些乱码文件，rm 删不掉，使用 find 命令删掉了

```bash
find -i
```
查询节点

```bash
find -inum [节点] -delete
```
根据节点删除，解决问题

### 2023/08/19 天气晴

今天使用 Vercel 部署 hugo 博客，结果报错
```
Error: add site dependencies: load resources: loading templates: "/vercel/workpath0/themes/meme/layouts/partials/third-party/lunr-search.html:8:1": parse failed: template: partials/third-party/lunr-search.html:8: function "warnf" not defined
```

查了一下，是 Vercel 默认使用的 hugo 版本太低了，需要指定版本，最好与建站时的版本一致，否则也可能会出问题

仓库根目录新建文件`vercel.json`

```json
{
    "build": {
      "env": {
        "HUGO_VERSION": "0.80.0"
      }
    }
  }
```