---
title: 解决 Vercel+Cloudflare 重定向次数过多的问题
tags: [Web,Cloudflare]
slug: 8d148259
date: 2023-05-07 17:43:16
---

>今天在使用 Cloudflare 做 OpenAPI 中转代理的时候将域名添加到了 Cloudflare 并配置了全球 CDN 加速，但是配置好之后域名无法访问了，浏览器报错为发生重定向太多，记录一下解决方法

## 修改加密模式

这是一个 Cloudflare 的经典 bug，使用 Vercel 和 Cloudflare 重复绑定时，需要将 Cloudflare 的 SSL/TLS 加密模式改为“完全”

在域名的概述页面侧边栏点击 SSL/TLS，选择“完全”模式即可解决

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/20230507212133.png)

## 检查 Proxy status

如果还是有这样的错误，在 DNS 中找到有问题的记录，检查 Proxy status 是否为 Proxied，如是，暂时关闭 Proxy（设置为 DNS only），然后点击右下角的保存

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/20230507212147.png)

之后尝试使用 nslookup 查询你自己的域名，如果依然是 Cloudflare 的 IP 地址，尝试清除 DNS 缓存（Windows 用户执行 ipconfig /flushdns）和更换 DNS 服务器为 [dns.google](https://link.zhihu.com/?target=http%3A//dns.google) 或 8.8.8.8 / 8.8.4.4，Google DNS 的记录更新速度实测非常快

![](https://jihulab.com/YuanJ2003/static/-/raw/main/blog-images/20230507212154.png)