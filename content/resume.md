---
title: Resume
---

#### 个人信息

<img src="/images/jiaoyuan.jpg" style="width:200px;border:10px solid rgba(255, 255, 255, 0);" align="right"/>

- 焦元/男
- 籍贯：甘肃敦煌
- 英语：CET-4
- 成都理工大学/环境科学与工程/2021 级本科
- 20 岁/2003 年
- Github: [YuanJ2003](https://github.com/YuanJ2003)
- 电话：17344338724
- 微信：Hieroglyphicss
- 博客：https://yuanj.top
- 公众号：[猫四叔](/images/wechat.jpg)
- 邮箱：hieroglyphs@foxmail.com

#### 个人技能

- 熟练使用 Git、GitHub 等代码工具
- 熟悉 Windows/Linux 操作系统的使用
- 熟悉基本的生物信息学分析
- 掌握 AutoCAD、Office 等常用软件的使用
- 掌握 Python 的基本使用
- 掌握 RNA-seq 上游分析部分
- 掌握简单的 HTML/CSS

#### 实践经历

- **2022.05-2022.06** 宜宾市 SYB 创新创业培训
- **2022.09-Now** 创立并运营公众号 [“猫四叔”](/images/wechat.jpg)
- **2022.12-2023.06** 全国大学生生命科学竞赛（科学探究类）/国二
- **2022.12-Now** 成都理工大学黄进课题组成员
- **2023.01-2023.05** 全国大学生市场调查与分析大赛/国三
- **2023.01-2023.05** 全国大学生电子商务“创新、创意及创业”挑战赛/省二
- **2023.01-Now** 搭建并运营博客 [“YuanJ 's Blog”](https://yuanj.top)
- **2023.04-Now** 搭建并运营 [“黄进课题组”](https://www.huangjin-lab.cn/) 主页

#### GitHub

<img src="http://ghchart.rshah.org/YuanJ2003" width="700px" align=center/>

- [HJLab](https://github.com/YuanJ2003/HJlab): 黄进课题组主页
- [Gene-family-identification](https://github.com/YuanJ2003/Gene-family-identification): 基因家族鉴定教程
- [auto-hmmer](https://github.com/YuanJ2003/auto-hmmer): 自动化基因家族鉴定
- [hexo-theme-hieroglyphs](https://github.com/YuanJ2003/hexo-theme-hieroglyphs): 极简 Hexo 主题
- [resume](https://github.com/YuanJ2003/resume): 个人简历模板
- [YuanJ2003](https://github.com/YuanJ2003/YuanJ2003): 基于 Hugo 的个人博客
- [OStools](https://github.com/YuanJ2003/OStools)：简易的水稻基因工具箱
