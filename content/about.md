---
title: About
---

<!-- <img src="/images/jiaoyuan.jpg" style="width:200px;border:10px solid rgba(255, 255, 255, 0);" align="right"/> -->

Hello 👋 I’m YuanJiao. I come from [Chengdu University of Technology](https://www.cdut.edu.cn/index.htm) and I am studying computer technology and bioinformatics .

You can find me at [Bilibili](https://space.bilibili.com/1692277850?spm_id_from=333.1007.0.0) or [WeChat Official Account](/images/weixin.jpg) and get more information.

At the same time, I have been studying and practicing at [Molecular Ecology&Molecular Toxicology Research Group](https://www.huangjin-lab.cn/) .

<!-- <img src="http://ghchart.rshah.org/YuanJ2003" width="700px" /> -->

## Contact

You can reach me by email at hieroglyphs AT foxmail.com. You can also connect with me on [WeChat](/images/Hieroglyphicss.jpg) and [GitHub](http://github.com/YuanJ2003) .